package com.gl.Restaurant.model;

import javax.persistence.*;

@Entity // Specifies that the class is an entity
@Table(name = "Items")
public class Items {
	@Override
	public String toString() {
		return "Items [itemId=" + itemId + ", itemName=" + itemName + ", prize=" + prize + "]";
	}

	@Id // primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ItemId", nullable = false)
	private int itemId;
	@Column(name = "ItemName", nullable = false)
	private String itemName;
	@Column(name = "Prize", nullable = false)
	private double prize;

	// generating field constructor
	public Items(int itemId, String itemName, double prize) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.prize = prize;
		// this.noOfItemsAvailable = noOfItemsAvailable;
	}

	// default constructor
	public Items() {

	}

	// generating getters and setters
	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getPrize() {
		return prize;
	}

	public void setPrize(double prize) {
		this.prize = prize;
	}

}
