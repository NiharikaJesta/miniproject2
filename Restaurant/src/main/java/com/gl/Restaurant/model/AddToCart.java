package com.gl.Restaurant.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity  //Specifies that the class is an entity
@Table
public class AddToCart {


	@Id //primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY)//Auto increment
	@Column(name = "OrderId", nullable = false)
	private int orderId;
	
	@Column(name = "ItemId", nullable = false)
	private int itemId;

	@Column(name = "ItemName", nullable = false)
	private String itemName;

	@Column(name = "Prize")
	private int prize;

	@Column(name = "Quantity",nullable = false)
	private int quntity;

	@Column(name="TotalPrice")
	private Integer totalPrice;
	
	@JsonFormat(pattern = "dd/mm/yyyy")
	@Column(name = "DateCreated")
	private LocalDate dateCreated;

	@Column(name = "Status")
	private String status;

	// default constructor
	public AddToCart() {
	}

	// field constructor

	public AddToCart(int orderId, int itemId, int userId, String itemName, int prize, int quntity,
			LocalDate dateCreated, String status) {
		super();
		this.orderId = orderId;
		this.itemId = itemId;
		this.itemName = itemName;
		this.prize = prize;
		this.quntity = quntity;
		this.dateCreated = dateCreated;
		this.status = status;
	}

	// generating getters and setters
	public int getOrderId() {
		return orderId;
	}
	
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	
	

	public Integer getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}

	public User getUserid() {
		return userid;
	}

	public void setUserid(User userid) {
		this.userid = userid;
	}

	
	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public int getPrize() {
		return prize;
	}

	public void setPrize(int prize) {
		this.prize = prize;
	}

	public int getQuntity() {
		return quntity;
	}

	public void setQuntity(int quntity) {
		this.quntity = quntity;
	}

	public LocalDate getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDate dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	//to String method
	@Override
	public String toString() {
		return "AddToCart [orderId=" + orderId + ", itemId=" + itemId + ", itemName=" + itemName + ", prize=" + prize
				+ ", quntity=" + quntity + ", totalPrice=" + totalPrice + ", dateCreated=" + dateCreated + ", status="
				+ status + ", userid=" + userid + "]";
	}
	
	//mapping relation between orderId and userId to create foreign key
	@ManyToOne
	@JoinColumn(name="userid", referencedColumnName = "userId") 
	private User userid;
	
	

}