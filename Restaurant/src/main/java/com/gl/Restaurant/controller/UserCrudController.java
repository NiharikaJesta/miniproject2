package com.gl.Restaurant.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.gl.Restaurant.Repositories.UserRepositories;

import com.gl.Restaurant.model.User;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.gl.Restaurant.service.UserCrudService;

@RestController
public class UserCrudController {

	@Autowired // getting object of User crudService
	UserCrudService userService;

	@Autowired // getting object of user repositories
	UserRepositories uRep;

	// mapping to user crude dashBoard
	@RequestMapping("/userCrudDashBoard")
	public ModelAndView getUserCrudDashBoard(HttpSession session) {
		List<User> userList = userService.getAllUsers();
		return new ModelAndView("UserCrudDashBoard", "userList", userList);
	}

	// mapping to get user form
	@GetMapping("/userForm")
	public ModelAndView getUserForm() {
		return new ModelAndView("UserForm", "userData", new User());
	}

	// mapping to save user
	@RequestMapping("/saveUser")
	public String saveUser(@ModelAttribute("userData") User u, HttpSession session) {
		userService.saveUser(u);
		return "redirect:/userCrudDashBoard";
	}

	// creating a request mapping that deletes a specified item
	@RequestMapping("/deleteuser/{userId}")
	public ModelAndView deleteUser(@PathVariable("userId") int userId) {
		System.out.println("user deleted");
		userService.deleteUser(userId);
		return new ModelAndView("redirect:/userCrudDashBoard");
	}

	// mapping to to get update form
	@RequestMapping("/updateUser/{userId}")
	public ModelAndView updateBook(@PathVariable("userId") int userId) {
		User u = uRep.getById(userId);
		return new ModelAndView("userUpdateForm", "auserData", u);
	}

	// mapping for update
	@RequestMapping("/Uupdated")
	public ModelAndView updated(@ModelAttribute("auserData") User user1) {
		userService.saveUser(user1);
		return new ModelAndView("redirect:/userCrudDashBoard");
	}

}
