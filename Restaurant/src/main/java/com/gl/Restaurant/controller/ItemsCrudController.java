package com.gl.Restaurant.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.gl.Restaurant.model.Items;
import com.gl.Restaurant.service.ItemsService;
import com.google.common.base.Optional;

@RestController
public class ItemsCrudController {
	@Autowired // getting object of itemService
	ItemsService itemService;

	// mapping to get item dashBoard
	@RequestMapping("/Itemdashboard")
	public ModelAndView getItemDashBoard() {
		List<Items> itemList = itemService.getAllItems();
		return new ModelAndView("itemBoard", "itemList", itemList);
	}

	// mapping to get item form
	@RequestMapping("/itemForm")
	public ModelAndView getItemForm() {
		return new ModelAndView("ItemForm", "itemData", new Items());
	}

	// mapping to save item
	@RequestMapping("/itemsSave")
	public String saveItems(@ModelAttribute("itemData") Items item, HttpSession session) {
		itemService.saveItems(item);
		return "redirect:/Itemdashboard";
	}

	// creating a request mapping that deletes a specified item
	@RequestMapping("/deleteitem/{itemId}")
	public ModelAndView deleteItem(@PathVariable("itemId") int itemId, HttpSession session) {
		System.out.println("item deleted");
		itemService.deleteItem(itemId);
		return new ModelAndView("redirect:/Itemdashboard");
	}

	// mapping to get update form
	@RequestMapping("/updateitem/{itemId}")
	public ModelAndView updateItem(@PathVariable("itemId") int itemId) {
		Items item = itemService.getItemId(itemId);
		return new ModelAndView("ItemUpdateForm", "itemData", item);
	}

	// mapping to update
	@RequestMapping("/update")
	public ModelAndView updateItem(@ModelAttribute("itemData") Items item, HttpSession session) {
		System.out.println("entered");
		Items i = itemService.getItemId(item.getItemId());
		itemService.saveItems(item);
		System.out.println("updated");
		return new ModelAndView("redirect:/Itemdashboard");
	}

}
