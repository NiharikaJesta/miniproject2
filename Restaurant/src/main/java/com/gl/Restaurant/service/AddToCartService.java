package com.gl.Restaurant.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.Restaurant.Repositories.AddToCartRepositories;
import com.gl.Restaurant.model.AddToCart;
import com.gl.Restaurant.model.User;

@Service // service where we write all business logic
public class AddToCartService {
	@Autowired
	public AddToCartRepositories addToCartRep;

	// method to get order id
	public AddToCart getOrderId(int id) {
		return addToCartRep.getById(id);
	}

	// method to get all orders in a cart
	public List<AddToCart> getAllOrders() {
		List<AddToCart> uList = new ArrayList<AddToCart>();
		addToCartRep.findAll().forEach(user -> uList.add(user));

		return uList;
	}

	// getting a specific record from AddToCart
	public AddToCart getAddToCartById(int id) {
		return addToCartRep.findById(id).get();
	}

	// Method to a order in AddToCart
		public AddToCart saveOrder(AddToCart addToCart) {
			addToCart.setDateCreated(LocalDate.now());
			return addToCartRep.save(addToCart);
		}


//	public void saveOrder(AddToCart addToCart)
//	{
//		List<AddToCart> list=getAllOrders();
//		for(AddToCart a:list ) {
//			if(a.getItemId()==addToCart.getItemId()) {
//				list.remove(addToCart);
//				break;
//			}
//			addToCart.setDateCreated(LocalDate.now());
//			list.add(addToCart);
//			addToCartRep.saveAll(list);
//		}
//		
//	}

	
	// delete a order from AddToCart
	public void deleteOrder(int id) {
		addToCartRep.deleteById(id);
		;
	}

	// update a order from AddToCart
	public void updateOrder(AddToCart addToCart) {
		addToCartRep.save(addToCart);
	}

}
