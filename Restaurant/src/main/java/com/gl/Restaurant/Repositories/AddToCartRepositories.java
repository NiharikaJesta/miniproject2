package com.gl.Restaurant.Repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gl.Restaurant.model.AddToCart;
import com.gl.Restaurant.model.User;

@Repository //that indicates that the present class is a repository
public interface AddToCartRepositories extends JpaRepository<AddToCart, Integer>{


	//  query to get list of add to cart
	@Query("select a from AddToCart a where a.userid=?1")
	public List<AddToCart> getcartDetails(User u);
	
	//  query to get sum of price of a add to cart
	@Query("select sum(a.totalPrice) from AddToCart a where a.userid=?1")
	public Integer getTotalPrice(User u);
	
	// query to get list of add to cart referred to localDate
	@Query("select a from AddToCart a where a.dateCreated=?1")
	public List<AddToCart> getTodayBill(LocalDate localDate);
	
	//  query to get list of add to cart referred to localDate month
	@Query("select a from AddToCart a where month(a.dateCreated)=?1")
	public List<AddToCart> getMonthlyBills(int month);

	
	//As an Admin I should be able to see all the orders done by a specific user.

	@Query("select a from AddToCart a join a.userid u where u.userId=?1")
	public List<AddToCart> ordersBySpecificUser(int u);
	
	
}









