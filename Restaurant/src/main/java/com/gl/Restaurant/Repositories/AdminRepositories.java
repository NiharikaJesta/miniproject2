package com.gl.Restaurant.Repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gl.Restaurant.model.Admin;
import com.gl.Restaurant.model.User;

@Repository // that indicates that the present class is a repository
public interface AdminRepositories extends JpaRepository<Admin, Integer> {

	// hibernate query to validate user admin login
	@Query("select admin from Admin admin where admin.email=?1 and admin.password=?2")
	public Optional<Admin> adminValidation(String email, String password);

}
