<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div align="center">
		<h2>******** WELCOME TO USER DASHBOARD ********</h2>


		<h3>
			<a href="addToCart-DashBoard"> Add To Cart</a>
		</h3>
<br><br>
		<h2>Item List</h2>

		<table border="1">

			<th>itemId</th>
			<th>itemName</th>
			<th>prize</th>
			<th>Action</th>

			<c:forEach var="contact" items="${itemList}" varStatus="status">
				<tr>

					<td>${contact.itemId}</td>
					<td>${contact.itemName}</td>
					<td>${contact.prize}</td>
					<td><a href="/buy/${contact.itemId}">Buy</a></td>
				</tr>

			</c:forEach>
		</table>

		<div align="justify"></div>

		<h3>
			<a href="logout">LOGOUT</a>
		</h3>
	</div>
</body>
</html>