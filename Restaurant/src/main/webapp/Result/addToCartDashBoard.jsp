<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<div align="center">
		<h1>AddToCart</h1>
	
		<table border="1">
			
			
			<th>ItemId</th>
			<th>ItemName</th>
			<th>Prize</th>
			<th>Quntity</th>
			<th>TotalPrice</th>
			<th>DateCreated</th>
			<th>UserId</th>
	<!-- 	<th>Update</th> -->
			<th>Remove</th>
			<!--  <th>ChechOut</th> -->

			<c:forEach var="contact" items="${addToCart}" varStatus="status">
				<tr>

					
					<td>${contact.itemId}</td>
					<td>${contact.itemName}</td>
					<td>${contact.prize}</td>
					<td>${contact.quntity}</td>
					<td>${contact.totalPrice}</td> 
					<td>${contact.dateCreated}</td>
					<td>${contact.userid.userId}</td>
					
			   <!-- <td><a href="/updateOrder/${contact.orderId}">Click</a></td> -->
					<td><a href="/deleteOrder/${contact.orderId}">Click</a></td>
					<!--  <td><a href="/checkOut/${contact.orderId}">Click</a>  -->
				</tr>
			
					<a href="/orderNow">Order-Now</a>
			</c:forEach>
		</table>
		
	
		<h3><a href="dashBoard">Continue Shopping</a></h3>
		<br><br>
		<h3>
			<a href="logout">LOGOUT</a>
		</h3>
	</div>

</body>
</html>