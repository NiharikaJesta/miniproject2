<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div align="center">
		<h1>****** Item List ******</h1>
		<h3>
			<a href="itemForm">New Item</a>
		</h3>
		<table border="1">
			
			<th>itemId</th>
			<th>itemName</th>
			<th>prize</th>
			<th>Action</th>

			<c:forEach var="contact" items="${itemList}" varStatus="status">
				<tr>

					<td>${contact.itemId}</td>
					<td>${contact.itemName}</td>
					<td>${contact.prize}</td>
					<td><a href="/updateitem/${contact.itemId}">Edit</a>
					 &nbsp;&nbsp;&nbsp;&nbsp;
					 <a href="/deleteitem/${contact.itemId}">Delete</a></td>
				</tr>
				
			</c:forEach>
		</table>
		
		<br>
		<h3><a href="backTotoAdminDashBoard">Back To Admin DashBoard</a></h3>
	</div>

</body>
</html>