<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>

<html>
   <head>
      <title>Spring MVC Form Handling</title>
   </head>

   <body>
   <div align="center">
  <h1> <form>***ORDER FORM***</form></h1>
      <form:form method = "POST" action = "/SAVEORDER" modelAttribute="orderDetails">
         <table>
         	
            <tr>
               <td><form:label path = "itemId">ItemId</form:label></td>
               <td><form:input path = "itemId" /></td>
            </tr>
            <tr>
               <td><form:label path = "itemName">itemName</form:label></td>
               <td><form:input path = "itemName" /></td>
            </tr>
            <tr>
               <td><form:label path = "Prize">Prize</form:label></td>
               <td><form:input path = "Prize" /></td>
            </tr>
            <h3 style="color: green;">select the quantity u want</h3>
            <tr>
               <td><form:label path = "quntity">Quantity</form:label></td>
               <td><form:input path = "quntity" /></td>
            </tr>
              
             <tr>
               <td><form:label path = "status">status</form:label></td>
               <td><form:input path = "status" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "ADDTOCART"/>
               </td>
            </tr>
         </table>  
      </form:form>
    </div>
   </body>
   
</html>